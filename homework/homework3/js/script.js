// Технические требования:
//
//   Считать с помощью модального окна браузера два числа.
//   Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено +, -, *, /.
// Создать функцию, в которую передать два значения и операцию.
//   Вывести в консоль результат выполнения функции.
//
//
//   Не обязательное задание продвинутой сложности:
//
//   После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа, - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).


function getNumber(index) {
  let number = prompt(`Enter the number${index}`, '0');
  while (number === '' || isNaN(number)) {
    number = prompt(`Enter the number${index}`, number);
  }
  return number
}

let firstNumber = getNumber(1);
let secondNumber = getNumber(2);
let operator = prompt('Enter the operation', '+');

function calc () {
  let result;
  switch (operator) {
    case '+' :
      result = parseFloat( firstNumber ) + parseFloat( secondNumber );
     break;
    case '-' :
      result  = parseFloat( firstNumber ) - parseFloat( secondNumber );
      break;
    case '*' :
      result  = parseFloat( firstNumber ) * parseFloat( secondNumber );

  }
  alert(`${firstNumber} ${operator} ${secondNumber} = ${result}`);
}

console.log(calc());