//  Задание
//
// Создать поле для ввода цены с валидацией.
//
//  Технические требования:
//   - При загрузке страницы показать пользователю поле ввода (`input`) с надписью `Price`. Это поле будет служить для ввода числовых значений
// - Поведение поля должно быть следующим:
//   - При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
// - Когда убран фокус с поля - его значение считывается, над полем создается `span`, в котором должен быть выведен текст: `Текущая цена: ${значение из поля ввода}`. Рядом с ним должна быть кнопка с крестиком (`X`). Значение внутри поля ввода окрашивается в зеленый цвет.
// - При нажатии на `Х` - `span` с текстом и кнопка `X` должны быть удалены. Значение, введенное в поле ввода, обнуляется.
// - Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - `Please enter correct price`. `span` со значением при этом не создается.
// - В папке `img` лежат примеры реализации поля ввода и создающегося `span`.



const inputPrice = document.createElement('input');
const container = document.createElement('div');
const priceField = document.createElement('span');
const delBtn = document.createElement('button');
const text = document.createElement('p');

document.querySelector('script').before(inputPrice);
container.appendChild(priceField);
priceField.after(delBtn);
inputPrice.before(container);
inputPrice.insertAdjacentHTML('beforebegin', '<label for="input-price">price, UAH</label>');
inputPrice.after(text);

delBtn.innerText = 'x';
text.innerText = 'Please enter correct price';
text.style.color = 'red';


container.hidden = false;
priceField.hidden = true;
delBtn.hidden = true;
text.hidden = true;

inputPrice.setAttribute('id', 'input-price');
inputPrice.classList.add('price-field');
container.classList.add('current-price-field');
priceField.classList.add('price-span');
delBtn.classList.add('del-btn');

inputPrice.addEventListener('blur', () => {
if (!isNaN(parseFloat(inputPrice.value))) {
  if (inputPrice.value < 0) {
    inputPrice.style.border = '2px solid red';
    container.hidden = false;
    priceField.hidden = true;
    delBtn.hidden = true;
    text.hidden = false;
    } else if (inputPrice.value === ''){
      inputPrice.style.border = '';
      container.hidden = false;
      priceField.hidden = true;
      delBtn.hidden = true;
      text.hidden = true;
      } else {
          priceField.innerText = `Current price: ${inputPrice.value}`;
          inputPrice.style.border = '';
          container.hidden = false;
          priceField.hidden = false;
          delBtn.hidden = false;
          text.hidden = true;
          }
}
});
inputPrice.addEventListener('focus', () => {
  inputPrice.style.border = '2px solid green';
  inputPrice.value = '';

});

delBtn.addEventListener('click', () => {
  container.hidden = false;
  priceField.hidden = true;
  delBtn.hidden = true;
  inputPrice.value = '0';
  inputPrice.style.border = '';
});


