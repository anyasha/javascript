// Технические требования:
//
//   Считать с помощью модального окна браузера число, которое введет пользователь.
//   Вывести в консоли все числа кратные 5, от 0 до введенного пользователем числа. Если таких чисел нету - вывести в консоль фразу `Sorry, no numbers'
// Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.
//
//
// Не обязательное задание продвинутой сложности:
//
// Проверить, что введенное значение является целым числом. Если данное условие не соблюдается, повторять вывод окна на экран до тех пор, пока не будет введено целое число.
// Считать два числа, m и n. Вывести в консоль все простые числа (http://ru.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE) в диапазоне от m до n (меньшее из введенных чисел будет m, бОльшее будет n). Если хотя бы одно из чисел не соблюдает условие валидации, указанное выше, вывести сообщение об ошибке, и спросить оба числа заново.


let endValue = prompt('Enter the number', '5');
while (!(parseInt(endValue) === parseFloat(endValue))) {
  endValue = prompt('Enter the number', endValue);
}

if (endValue < 5) {
  confirm('Sorry, no numbers')
}
for (let i = 5; i <= endValue; i+=5 ) {
  if (i % 5 === 0) {
    console.log(`${i} - число кратно 5 в диапазоне от 0 до ${endValue}`);
  }
}
//
console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

let m = prompt('Enter first number', '3');
  while (!(parseInt(m) === parseFloat(m))) {
    m = prompt('Enter first number', m);
  }
let n = prompt('Enter last number', '15');
  while (!(parseInt(n) === parseFloat(n))) {
    n = prompt('Enter last number', n);
  }

    if (n < m ) {
      let k = n;
      n = m;
      m = k;
    }

for (let i = m; i <= n; i++) {

    if ( i >= 1 && i<= 3 ) {
      console.log(`${i} - простое число в диапазоне от ${m} до ${n}`)
    }
  for ( let j = 2; j <=  Math.sqrt(i); j++ ) {
    if ( i % j === 0 )
      break;
    if ( j === Math.floor( Math.sqrt(i) )) {
      console.log(`${i} - простое число в диапазоне от ${m} до ${n}`)
    }
  }
}