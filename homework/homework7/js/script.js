// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка.
//
//   Технические требования:
//
//
//   Создать функцию, которая будет принимать на вход массив.
//   Каждый из элементов массива вывести на страницу в виде пункта списка
// Необходимо использовать шаблонные строки и функцию map массива для формирования контента списка перед выведением его на страницу.
//   Примеры массивов, которые можно выводить на экран:
//   ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
//     ['1', '2', '3', 'sea', 'user', 23]
// Можно взять любой другой массив.

const array = ['hello', 'world', 'Kiev', 'Kharkiv', [1, 2, 3], 'Odessa', 'Lviv'];

const list = document.createElement ('ul');
document.querySelector('script').before(list);

function createList(array) {
  array.map((elem) => {
      const li = document.createElement('li');
      if (Array.isArray(elem)) {
        const attachedUl = document.createElement('ul');
        elem.map((item) => {
          const li = document.createElement('li');
          li.innerText = item;
          attachedUl.appendChild(li)
        });
        li.appendChild(attachedUl)
      }

      else
        li.innerText = elem;
      document.querySelector('ul').appendChild(li);
  })
}
console.log(createList(array));



