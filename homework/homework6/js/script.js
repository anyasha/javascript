// Задание
// Реализовать функцию фильтра массива по указанному типу данных.
//
//   Технические требования:
//
//   Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
  // Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом. То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string', то функция вернет массив [23, null].
function filterBy(array = ['hello', 'world', 23, '23', null], dataType = 'string') {
  let filterArray = array.filter((element) => {
    if (dataType === 'array' && Array.isArray(element))
      return false;
    else
      return typeof element !== dataType;
  });
  console.log(array);console.log(` was filtered by data type ${dataType}:`);
  return filterArray
}

console.log(filterBy());
console.log('````````````````````````````````````````````````````````````````');
console.log(filterBy(['hello', 'world', 23, '23', null], 'number'));
console.log('````````````````````````````````````````````````````````````````');
console.log(filterBy(['hello', 'world', 23, '23', null, true], 'boolean'));
console.log('````````````````````````````````````````````````````````````````');
console.log(filterBy(['hello', 'world', 23, '23', null, true, {name: "John", age: 30}], 'object'));
console.log('````````````````````````````````````````````````````````````````');
console.log(filterBy(['hello', 'world', 23, '23', null, true, {name: "John", age: 30}, [1, 2, 3, 4]], 'array'));
console.log('````````````````````````````````````````````````````````````````');
