// Задание
// Создать объект студент "студент" и проанализировать его табель.
//
//   Технические требования:
//
//   Создать пустой объект student, с полями name и last name.
//   Спросить у пользователя имя и фамилию студента, полученные значения записать в соответствующие поля объекта.
//   В цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. Записать оценки по всем предметам в свойство студента tabel.
//   Посчитать количество плохих (меньше 4) оценок по предметам. Если таких нет, вывести сообщение Студент переведен на следующий курс.
//   Посчитать средний балл по предметам. Если он больше 7 - вывести сообщение Студенту назначена стипендия.

let student = {};
student.name = prompt('What is your name?', '');
student.lastName = prompt('What is your last name?', '');
student.table = [];

let subject = '';
let mark = '';
do {
  subject = prompt('What is the subject name?', '');
  if (subject === null) {
    break;
  }
  mark = prompt('What is the subject mark?', '');
  if (mark === null) {
    break;
  }
  student.table.push(parseInt(mark));
}
while ((subject !== null));


let badMarks = 0;
student.table.forEach((element) => {
  if (element < 4) {
    badMarks++;
  }
 });
if (badMarks === 0 && student.table.length > 0) {
  alert(`Студент ${student.name} ${student.lastName} переведен на следующий курс`);
}
else {
  console.log(`Количество оценок меньше 4 - ${badMarks}`);
}

let averageMark = 0;
student.table.forEach((element) => {
  averageMark +=element/student.table.length;
  });
  if (averageMark > 7) {
    alert(`Студенту ${student.name} ${student.lastName} назначена стипендия`)
  }
console.log(student);
console.log(student.table);
console.log(`Средний балл ${averageMark}`);


