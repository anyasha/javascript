// ## Задание
//
// Нарисовать на странице круг используя параметры, которые введет пользователь.
//
//   #### Технические требования:
//   - При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". Данная кнопка должна являться единственным контентом в теле HTML документа, весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript.
// - При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться, то есть все остальные круги сдвигаются влево.
// - У вас может возникнуть желание поставить обработчик события на каждый круг для его исчезновения. Это неэффективно, так делать не нужно. На всю страницу должен быть только один обработчик событий, который будет это делать.

const drawCircleBtn = document.getElementById('draw-circle-btn');
const diameterField = document.createElement('input');
const drawBtn = document.createElement('button');

diameterField.placeholder = 'Введите диаметр круга'

drawCircleBtn.addEventListener('click', (event) => {
  drawCircleBtn.after(diameterField);
  diameterField.after(drawBtn);
  drawBtn.classList.add('btn');
  drawBtn.innerText = 'Нарисовать'
});

drawBtn.addEventListener('click', () => {
  const diameterCircle = diameterField.value;
  let circleContainer = document.querySelector('.wrapper');
  console.log(circleContainer);
  if (!document.getElementsByClassName('wrapper').length) {
    drawCircle(diameterCircle, 10, 10)
    } else {
      circleContainer.remove();
      drawCircle(diameterCircle, 10, 10)
    }
circleContainer = document.querySelector('.wrapper');
circleContainer.addEventListener('click', (event) => {
    if (event.target.classList.contains('circle')) {
const clickedCircle = event.target;
clickedCircle.remove()
    }
  })
});


function drawCircle(diameter, numberRow, numberColumn) {
  const wrapper = document.createElement('div');
  drawBtn.after(wrapper);
  wrapper.classList.add('wrapper');
  wrapper.style.width = diameter * numberRow + 'px';
  for (let i = 0; i < numberRow * numberColumn; i++) {
    const circle = document.createElement('div');
    wrapper.appendChild(circle);
    circle.classList.add('circle');
    circle.style.width = diameter + 'px';
    circle.style.height = diameter + 'px';
    circle.style.backgroundColor = getRandomColor();
  }
}

function getRandomColor() {
  const colorR = Math.floor(Math.random()*255);
  const colorG = Math.floor(Math.random()*255);
  const colorB = Math.floor(Math.random()*255);
  return `rgb(${colorR}, ${colorG}, ${colorB})`
}
