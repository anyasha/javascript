// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
//
//   Технические требования:
//
//   Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
//
//   При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
//   Создать метод getAge() который будет возвращать сколько пользователю лет.
//   Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
//
//
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.
function createNewUser() {
  const newUser = {
    firstName: prompt('Enter your name', ''),
    lastName: prompt('Enter your last', ''),
    birthday: prompt('Enter your birthday', 'dd.mm.yyyy'),
    getLogin () {
      let userLogin = this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
      alert(userLogin);
      return userLogin;
    },
    // getAge () {
    //   const dateBirthday = this.birthday.split('.').reverse().join('.');
    //   let age = Math.floor ((Date.now() - Date.parse(dateBirthday))/31540000000);
    //   return age;
    // }

    getAge() {
      const dateBirthday = new Date( this.birthday.split('.').reverse().join('.'));
      let age = new Date().getFullYear() - dateBirthday.getFullYear();
      if (new Date().getMonth() - dateBirthday.getMonth() < 0) {
        age--;
      }
      else if (dateBirthday.getMonth() === new Date().getMonth())
      {
        if (new Date().getDate() - dateBirthday.getDate() < 0 ) {
          age--;
        }
      }
      return age;
    },

    getPassword() {
      let name = user.getLogin().substring(0,1).toUpperCase() + user.getLogin().substring(1).toLowerCase();
      let year = user.birthday.substring(6);
      return name + year;
    }
  };
  return newUser;
}

let user = createNewUser();
console.log(`User login: ${user.getLogin()}`);
console.log(`User age: ${user.getAge()}`);
console.log(`User password: ${user.getPassword()}`);
