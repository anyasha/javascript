

const pacMan = document.querySelector('.pacman');
document.addEventListener('keydown', (event) => {
  let keyName = event.key;

  switch (keyName) {
    case 'ArrowRight':
      if (+pacMan.style.left.split('px')[0] < +(document.documentElement.clientWidth - document.querySelector('.pacman').clientWidth)) {
        pacMan.style.left = (`${+pacMan.style.left.split('px')[0] + 50}px`);
      }
      break;
    case 'ArrowDown':
      if (+pacMan.style.top.split('px')[0] < +(document.documentElement.clientHeight - document.querySelector('.pacman').clientHeight)) {
    pacMan.style.top = (`${+pacMan.style.top.split('px')[0] + 50}px`);
      }
      break;
    case 'ArrowLeft':

      if (+pacMan.style.left.split('px')[0] > 0) {
        pacMan.style.left = (`${+(pacMan.style.left.split('px')[0]) - 50}px`);
        console.log(pacMan.style.left);
      }
      break;
    case 'ArrowUp':
      if ( +pacMan.style.top.split('px')[0] > 0) {
        pacMan.style.top = (`${+pacMan.style.top.split('px')[0] - 50}px`);
        console.log(pacMan.style.top);
      }
      break;
  }
});