// Технические требования:
//
//   Добавить вверху страницы горизонтальное меню со ссылкой на все разделы страницы.
//   При клике на ссылку - плавно прокручивать страницу вниз до выбранного места.
//   Если прокрутить страницу больше чем на 1 экран вниз, справа внизу должна появляться кнопка "Наверх" с фиксированным позиционариванием. При клике на нее - плавно прокрутить страницу в самый верх.
//   Добавить под одной из секций кнопку, которая будет выполнять функцию slideToggle() (прятать и показывать по клику) для данной секции.
$(document).ready(function(){
$('a[href*="#"]').click(function () {
  const $anchor = $(this).attr('href');
  $('html, body').animate({
    scrollTop: $($anchor).offset().top
  }, 900);
  return false;
});
$(document).scroll(function() {
  const $screenHeight = $(window).innerHeight();
  const $screenTop = $(window).scrollTop();
  if ($screenTop > $screenHeight) {
    if (!$('.scroll-top-btn').length) {
      const $scrollTopButton = $('<button hidden class="scroll-top-btn"></button>');
      $('script:first').before($scrollTopButton);
      $scrollTopButton.fadeIn();
      $scrollTopButton.click(function (){
        $('html, body').animate({
          scrollTop: 0
        }, 1000);
      });
    }
    } else {
      $('.scroll-top-btn').fadeOut(300, () => {
        $('.scroll-top-btn').remove()
      })
  }
});
$('#slide-toggle-btn').click(function () {
  $('.post-gallery').slideToggle('slow');
  $('html, body').animate({
    scrollTop: 0
  }, 500);
})
});

