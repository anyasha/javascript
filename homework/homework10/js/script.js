// В файле index.html лежит разметка для двух полей ввода пароля.
//  - По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид.
//  - Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
//  - Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
//  - По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
//  - Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
//  - Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения
//  -
//  - После нажатия на кнопку страница не должна перезагружаться
//  - Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.

const pswForm = document.querySelector('.password-form');
const pswInput = document.getElementById('psw-input');
const pswRepeatInput = document.getElementById('repeat-psw-input');

pswForm.addEventListener('click', event => {
  if (event.target.classList.contains('fa-eye')) {
    showPassword(event)
  } else if (event.target.classList.contains('fa-eye-slash')) {
    hidePassword(event)
  } else if (event.target.classList.contains('btn')) {
    comparePassword()
  }
});

function showPassword(browserEvent) {
    const clickedElement = browserEvent.target;
    clickedElement.previousElementSibling.setAttribute('type', 'text');
    clickedElement.classList.replace('fa-eye', 'fa-eye-slash')
}

function hidePassword(browserEvent) {
  const clickedElement = browserEvent.target;
  clickedElement.previousElementSibling.setAttribute('type', 'password');
  clickedElement.classList.replace('fa-eye-slash', 'fa-eye')
}

  let wrongPswText = null;
function comparePassword(){
  if (pswInput.value === '' || pswInput.value.search(/\s/) >= 0 || pswRepeatInput.value.search(/\s/) >= 0) {
    wrongPswText.remove();
    wrongPswText = null;
    alert('Password must contain characters and cannot contain spaces')
  }
    else if (pswInput.value === pswRepeatInput.value) {
    wrongPswText.remove();
    wrongPswText = null;
    alert('You are welcome');

  }

    else if (wrongPswText == null){
     wrongPswText = document.createElement('p');
        pswRepeatInput.nextElementSibling.after(wrongPswText);
        wrongPswText.innerText = 'Wrong Password';
        wrongPswText.style.color = 'red';
        wrongPswText.style.paddingLeft = '150px'
      }
}
