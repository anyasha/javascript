// - В папке `tabs` лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
// - Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// - Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.


const menu = document.querySelector('.tabs');
const content = Array.from(document.querySelectorAll('.content'));
console.log(content);
menu.addEventListener('click', (event) => {
  const clickedItem = event.target;
  const activeItemMenu = document.querySelector('.active');
  if (clickedItem.classList.contains('tabs-title')) {
    clickedItem.classList.toggle('active');
    activeItemMenu.classList.remove('active');
    const data = clickedItem.dataset.tab;
    console.log(data);
    content.forEach(elem => {
      elem.classList.remove('active');
      if(elem.dataset.tab === data){
        elem.classList.add('active')

      }
    })
  }
});


